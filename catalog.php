<html>
<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

        <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

        <script type="text/javascript">
    		document.addEventListener("DOMContentLoaded", () => {
    			var swiper1 = new Swiper('.swiper-container1', {
    				loop: true,
    				slidesPerView: 3,
    				spaceBetween: 30,
    				centeredSlides: false,

                    navigation: {
    				nextEl: '.swiper-button-next',
    				prevEl: '.swiper-button-prev',
    				},

    				breakpoints: {
    					280: {
    						slidesPerView: 1,
    						spaceBetween: 1
    					},
    					320: {
    						slidesPerView: 1,
    						spaceBetween: 5
    					},
    					760: {
    						slidesPerView: 3,
    						spaceBetween: 5
    					},
    					780: {
    						slidesPerView: 3,
    						spaceBetween: 30
    					},
    				}
    			})
    		});
    	</script>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/Lovelace/lovelace.css">
        <link rel="stylesheet" type="text/css" href="fonts/Montserrat/montserrat.css">

</head>
<body>

    <?php
    include 'header.php';
    ?>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="header">Кольца из золота</h2>
                <h3 class="second-header">Подборка актуальных авторских дизайнов колец</h3>
            </div>
        </div>
        <div class="row swiper-container swiper1 py-5">
            <div class="col-12">
                <div class="swiper-container1 swiper1">
                  <div class="swiper-wrapper">
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/silver.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/gold.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/bronze.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="swiper-button-prev hide-for-small-only hide-for-medium-only"></div>
                    <div class="swiper-button-next hide-for-small-only hide-for-medium-only"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="header">Кольца из серебра</h2>
                <h3 class="second-header">Подборка актуальных авторских дизайнов колец</h3>
            </div>
        </div>
        <div class="row swiper-container swiper1 py-5">
            <div class="col-12">
                <div class="swiper-container1 swiper1">
                  <div class="swiper-wrapper">
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/silver.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/gold.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/bronze.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="swiper-button-prev hide-for-small-only hide-for-medium-only"></div>
                    <div class="swiper-button-next hide-for-small-only hide-for-medium-only"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="header">Кольца из бронзы</h2>
                <h3 class="second-header">Подборка актуальных авторских дизайнов колец</h3>
            </div>
        </div>
        <div class="row swiper-container swiper1 py-5">
            <div class="col-12">
                <div class="swiper-container1 swiper1">
                  <div class="swiper-wrapper">
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/silver.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/gold.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="swiper-slide">
                          <div class="card shop-card img-fluid d-block m-0-auto">
                              <img class="card-img-top" src="img/bronze.png">
                              <div class="card-body card-body-position">
                                  <div class="card-text text-center">
                                      <p>
                                        BULGARI LION STACK
                                      </p>
                                      <p>
                                          р. 5000
                                      </p>
                                      <button href="#" class="btn rings-button" role="button">Заказать кольца</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="swiper-button-prev hide-for-small-only hide-for-medium-only"></div>
                    <div class="swiper-button-next hide-for-small-only hide-for-medium-only"></div>
                </div>
            </div>
        </div>
    </div>

    <?php
    include 'personal.php';
    include 'footer.php';
    ?>

</body>
</html>
