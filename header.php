<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="stylesheet" type="text/css" href="fonts/Lovelace/lovelace.css">
    <link rel="stylesheet" type="text/css" href="fonts/Montserrat/montserrat.css">

</head>
<body>

<div class="container pt-3">
    <div class="row">
        <div class="col-md">
            <p class="name">
                Just Diamond
            </p>
        </div>
        <div class="col-md">
            <a href="index.php">
                <p class="second-header" style="float: right;">
                    Главная
                </p>
            </a>
        </div>
        <div class="col-md">
            <a href="catalog.php">
                <p class="pl-2 second-header text-center">
                    Каталог
                </p>
            </a>
        </div>
        <div class="col-md">
            <a href="contacts.php">
                <p class="pl-4 second-header" style="float: left;">
                    Контакты
                </p>
             </a>
        </div>
        <div class="col-md">
            <p class="second-header" style="float: right;">
                +7 999 999 99 99
            </p>
        </div>
    </div>
</div>
</body>
</html>
