<html>
<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/Lovelace/lovelace.css">
        <link rel="stylesheet" type="text/css" href="fonts/Montserrat/montserrat.css">

</head>
<body>
<section class="au">
        <div class="row">
            <div class="col-6">
                <img style="width: 100%;" src="img/au.png">
            </div>
            <div class="col-6 px-5" style="padding-top:3rem;">
                <div class="pt-5 mb-3">
                    <h2 class="au-header mb-3">Вход в личный кабинет</h2>
                    <p><input class="input-field mt-4 mb-3" name="email" placeholder="Введите Ваш email"></p>
                    <p><input class="input-field mb-4" name="pass" type="password" placeholder="Введите Ваш пароль"></p>
                    <button href="#" srtyle="width:1rem;" class="btn au-button" role="button">Войти</button>
                </div>
            </div>
        </div>
</section>
</body>
</html>
