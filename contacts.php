<html>
<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" type="text/css" href="fonts/Lovelace/lovelace.css">
        <link rel="stylesheet" type="text/css" href="fonts/Montserrat/montserrat.css">
</head>
<body>

    <?php
    include 'header.php';
    ?>

<section class="contacts">
    <div class="row py-5 px-5">
        <div class="col-12">
            <h2 class="contacts-header">Контакты</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <p class="contacts-text pl-5">+7 999 999 99 99</p>
            <p class="contacts-text pl-5">just-diamond@mail.ru</p>
            <p class="contacts-text pl-5">ТРЦ "Акварель"</p>
            <p class="contacts-text pl-5">г.Волгоград, Университетский просп., 107</p>
            <p class="contacts-text pl-5">Режим работы: пн-пт с 9:00 до 18:00</p>
            <a href="index.php">
                <button class="btn au-button ml-5" role="button">Вернуться на главную</button>
            </a>
        </div>
        <div class="col-6">
            <img class="contacts-img" src="img/contacts.jpg">
        </div>
    </div>
</section>

<section>
    <div style="position:relative;overflow:hidden;"><a href="https://yandex.ru/maps/org/akvarel/1231824218/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Акварель</a><a href="https://yandex.ru/maps/38/volgograd/category/shopping_mall/184108083/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:14px;">Торговый центр в Волгограде</a><a href="https://yandex.ru/maps/38/volgograd/category/entertainment_center/184106372/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:28px;">Развлекательный центр в Волгограде</a><iframe src="https://yandex.ru/map-widget/v1/-/CCUEMQX61B" width="100%" height="300" allowfullscreen="true" style="position:relative;"></iframe></div>
</section>

<?php
include 'footer.php';
?>

</body>
</html>
